import React from "react";
import { theme } from "./theme";
import {
  Box,
  CssBaseline,
  Stack,
  ThemeProvider,
} from "@mui/material";
import Navbar from "./Components/shared/Navbar";
import FilterSection from "./Components/filters/filterSection";
import GridSection from "./Components/grid/GridSection";
import { Route, Routes } from "react-router-dom";
import UserProfile from "./Components/user/UserProfile";
import Favorites from "./Components/user/Favorites";
import CompareView from "./Components/grid/CompareView";
import { useQuery } from "react-query";
import { VrScansAPI } from "../src/api/vrScansAPI";
import { useDispatch, useSelector } from "react-redux";
import { userActions } from "../src/store/users-slice";
import { vrScansActions } from "../src/store/vrScans-slice";
import Login from "./Components/formComponents/Login";
import Signup from "./Components/formComponents/Signup";
import formJSONlogin from "./formDataLogin.json";
import formJSONsignup from "./formDataSignup.json";

function App() {
  const dispatch = useDispatch();
  const user = useSelector((state) => state.userReducer.user);
  const users = useSelector((state) => state.userReducer.users);
  const vrScans = useSelector((state) => state.vrScansReducer.vrScans);
  const manufacturers = useSelector(
    (state) => state.vrScansReducer.manufacturers,
  );
  useQuery(["users"], () => VrScansAPI.getAllItems("/users"), {
    enabled: users.length === 0,
    onSuccess: (data) => {
      dispatch(userActions.setUsers(data));
    },
  });

  useQuery(["vrScans"], () => VrScansAPI.getAllItems("/vrscans"), {
    enabled: vrScans.length == 0,
    onSuccess: (data) => {
      dispatch(vrScansActions.setVRScans(data));
      dispatch(vrScansActions.setFilteredVrScans(data));
    },
  });

  useQuery(["manufacturer"], () => VrScansAPI.getAllItems("/manufacturers"), {
    enabled: manufacturers.length == 0,
    onSuccess: (data) => {
      dispatch(vrScansActions.setManufacturers(data));
    },
  });

  console.log(user)
  useQuery(["favorites"], () => VrScansAPI.getAllItems("/favorites?userId=" + user.id), {
    enabled: user != null && user.email != null,
    onSuccess: (data) => {

      dispatch(userActions.setFavorites(data));
    },
  });
  //
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline disableGutters sx={{ m: "0 auto" }}>
        <Navbar />
        <Box maxWidth={1280} sx={{ m: "0 auto" }}>
          <Stack direction="row" spacing={1} justifyContent="center">
            <Routes>
              <Route
                exact
                path="/"
                element={
                  <>
                    <FilterSection />
                    <GridSection />
                  </>
                }
              />
              <Route
                exact
                path="/login"
                element={
                  <>
                    <Login
                      formJSONlogin={formJSONlogin}
                      onSubmit={(data) => {
                        console.log(data);
                      }}
                    />
                  </>
                }
              />
              <Route
                exact
                path="/register"
                element={
                  <>
                    <Signup
                      formJSONsignup={formJSONsignup}
                      onSubmit={(data) => {
                        console.log(data);
                      }}
                    />
                  </>
                }
              />
              <Route exact path="/user" element={<UserProfile />} />
              <Route exact path="/favorites" element={<Favorites />} />
              <Route exact path="/compare" element={<CompareView />} />
            </Routes>
          </Stack>
        </Box>
      </CssBaseline>
    </ThemeProvider>
  );
}

export default App;
