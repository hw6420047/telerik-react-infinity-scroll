import { vrScansAxiosClient } from "./config/axiosConfig";

export const VrScansAPI = {
  getAllItems: async function (url) {
    const response = await vrScansAxiosClient.request({
      url: url,
      method: "GET",
    });
    return response.data;
  },
  updateItem: async function (url, data, action) {
    const response = await vrScansAxiosClient.request({
      url: url,
      data: data,
      method: action,
    });
    return response.data;
  },

  deleteItem: async function(url, token) {
    const response = await vrScansAxiosClient.request({
      url: url,
      headers: {
        Authorization: "Bearer " + token
      },
      method: "DELETE",
    });
    return response.data;
  },

  createUser: async function (url, data) {
    const response = await vrScansAxiosClient.request({
      url: url,
      data: data,
      method: "POST",
    });
    return response.data;
  },
  loginUser: async function (url, data) {    
    const response = await vrScansAxiosClient
      .request({
        url: url,
        data: data,
        method: "POST",
      })

    if (response.data.accessToken)
      localStorage.setItem("user", JSON.stringify(response.data));

    return response.data;
  },
  editUser: async function (url, data, token) {    
    const response = await vrScansAxiosClient
      .request({
        url: url,
        data: data,
        method: "PUT", 
        headers: {
          'content-type': "application/json",
          "Authorization": "Bearer " + token
        }
      })

    if (response.data.accessToken)
      localStorage.setItem("user", JSON.stringify(response.data));

    return response.data;
  },
  logoutUser: function () {
    localStorage.removeItem("user");
  },
};
