import axios from "axios";

const errorHandler = (error) => {
  return Promise.reject(error);
};

export const vrScansAxiosClient = axios.create({
    //baseURL: "https://api-telerik-project.vercel.app/",
    baseURL: "http://localhost:4200/"
});

vrScansAxiosClient.interceptors.response.use(undefined, (error) => {
  return errorHandler(error);
});
