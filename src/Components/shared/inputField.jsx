import React from "react";

const InputField = (inputElement, handleInputChange, setInputs, inputValue) => {
  const id = inputElement.id;

  return (
    <div className="form-group" key={id + "-wrapper"}>
      <label htmlFor={id} key={id + "-label"}>
        {inputElement.label}
      </label>
      {inputElement.required ? "*" : ""}
      <input
        type={inputElement.type}
        id={id}
        key={id + "-element"}
        className="form-control"
        onChange={handleInputChange}
        placeholder={inputElement.placeholder}
        value={inputValue || ""}
        required={inputElement.required}
      />
    </div>
  );
};

export default InputField;
