import React from 'react';
import InputField from './inputField';
import SelectField from './selectField';
import CheckboxField from './checkboxField';
import { useState } from "react";

const DynamicFormBuilder = (props) => {
    const [inputs, setInputs] = useState({});
    const formJSON = props.formJSON;

    const renderElement = (formElement) => {
        let type = formElement.type;
        switch(type) {
            case 'text':
                return InputField(formElement, handleInputChange, setInputs, inputs[formElement.id]);
             case 'select':
                 return SelectField(formElement, handleInputChange, setInputs, inputs[formElement.id]);
             case 'checkbox':
                 return CheckboxField(formElement, handleInputChange, setInputs, inputs[formElement.id], inputs);
            default:
                console.log('Unknown type ' + type);
                break;
        } 
    }

    const handleInputChange = (event) =>  {
        const target = event.target;
        let value = target.value;
        const id = target.id;

        if(target.type === 'checkbox') {
            value = target.checked;
        }

        setInputs(values => ({...values, [id]: value}))
      }

    const handleSubmit = (e) => {
        e.preventDefault();
        const result = JSON.stringify(Object.entries(inputs).map(s => ({[s[0]] : s[1]})))
        console.log(result);
      }

    return (<form onSubmit={handleSubmit} key="form"> {formJSON.fields.map(e => renderElement(e))} <button key="form-button" type="submit" className="btn btn-primary mb-2">Submit</button></form>);
}

export default DynamicFormBuilder;