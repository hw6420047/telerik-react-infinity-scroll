import { theme } from "../../theme";
import { useSelector } from "react-redux";
import { Box, InputBase, styled } from "@mui/material";
import React from "react";

const SearchField = styled("div")({
    margin: "0 auto",
    backgroundColor: "white",
    borderRadius: 5,
    
});
//
function SearchBar(props) {
    const inputValue = useSelector((state) => state.filtersReducer.inputSearchbox);
    return (
        <Box>
            <SearchField
                sx={{
                    border: 2,
                    borderColor: theme.palette.primary.light,
                    px: "4px"
                }}
            >
                <InputBase onChange={props.onChange} placeholder="Search by name..." value={inputValue || ''}/>
            </SearchField>
        </Box>
    );
}

export default SearchBar;
