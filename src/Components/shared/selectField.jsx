import React from 'react';

function SelectField(selectElement, handleInputChange, setInputs, inputValue) {
    let id = selectElement.id;

    const renderOption = (optionValue) => {
        return (
            <option key={optionValue}>{optionValue}</option>
        )
    }

        return (
            <div className="form-group" key={id + "-wrapper"}>
                <label key={id + "-label"} htmlFor={id}>{selectElement.label}</label>{selectElement.required ? '*' : ''}
                <select key={id + "-select-wrapper"} className="form-control" id={id} value={selectElement.value || ''}  onChange={handleInputChange} required={selectElement.required}>
                    {selectElement.options.map(o => renderOption(o.label))}
                </select>
            </div>
        )
}

export default SelectField;