import React from 'react';

function CheckboxField(checkboxElement, handleInputChange, setInputs, inputValue) {
    const checkboxId = checkboxElement.id;

    const renderHiddenInput = () => {
        if(checkboxElement.showInput) {
        let isVisible = false;
        
        if(inputValue != undefined) {
            isVisible = inputValue;
        }

        const inputName = checkboxElement.inputName;
        const id = checkboxElement.id + inputName;

        return(<div className="form-group" key={id + "-wrapper"} hidden={!isVisible}>
                <label key={id + "-label"} htmlFor={id}>{inputName}*</label>
                <input key={id + "-element"} className="form-control" type={checkboxElement.inputType} id={id} onChange={handleInputChange} required={isVisible}/>
            </div>);
        } else {
            return '';
        }
    }

    return (<div key={checkboxId + '-group-wrapper'}>
            <div className="form-group" key={checkboxId + "-wrapper"}>
                <input key={checkboxId + "-element"} className="form-check-input" type="checkbox" value={checkboxElement.value || ''} id={checkboxId} onChange={handleInputChange} required={checkboxElement.required}/>
                <label key={checkboxId + "-label"} className="form-check-label" htmlFor={checkboxId}>{checkboxElement.required ? '*' : ''}
                {checkboxElement.label}
                </label>
            </div>
                {renderHiddenInput()}
            </div>)
}

export default CheckboxField;