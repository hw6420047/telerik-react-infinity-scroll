import {
  AppBar,
  Avatar,
  Box,
  Button,
  Container,
  Toolbar,
  Tooltip,
  Typography,
  styled,
} from "@mui/material";
import LogoutIcon from "@mui/icons-material/Logout";
import FavoriteIcon from "@mui/icons-material/Favorite";
import { theme } from "../../theme";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { VrScansAPI } from "../../api/vrScansAPI";
import { userActions } from "../../store/users-slice";
import { filtersActions } from "../../store/filters-slice";
import {vrScansActions} from "../../store/vrScans-slice";

const StyledToolbar = styled(Toolbar)({
  display: "flex",
  justifyContent: "space-between",
});

const Icons = styled(Box)(({ theme }) => ({
  display: "none",
  gap: "20px",
  alignItems: "center",
  [theme.breakpoints.up("sm")]: {
    display: "flex",
  },
}));

function Navbar() {
  const dispatch = useDispatch();
  const user = useSelector((state) => state.userReducer.user);

  function removeUser() {
    dispatch(userActions.setUser());
    dispatch(userActions.setFavorites([]));
    dispatch(filtersActions.setSelectedColors([]));
    dispatch(filtersActions.setSelectedTypes([]));
    dispatch(filtersActions.setSelectedTags([]));
    dispatch(filtersActions.setInputSearchbox(''));
    dispatch(vrScansActions.setFilteredVrScans([]));
    dispatch(vrScansActions.setCompareItems([]));
    VrScansAPI.logoutUser("");
  }

  return (
    <AppBar
      position="sticky"
      sx={{
        bgcolor: theme.palette.primary.light,
        color: theme.palette.primary.doubleDark,
        borderBottom: 1,
        borderBottomColor: theme.palette.secondary.main,
        mb:3
      }}
    >
      <Container>
        <StyledToolbar>
          <Typography
            variant="h5"
            sx={{
              m: { xs: "0 auto", sm: 0 },
              fontWeight: [500],
            }}
          >
            <Link to="/" style={{ color: theme.palette.primary.doubleDark }}>
              VR Scans
            </Link>
          </Typography>
          {!user?.id && (
            <Icons sx={{ display: { xs: "none", sm: "inherit" } }}>
              <Link to="/login">
                <Button
                  variant="text"
                  sx={{
                    color: theme.palette.primary.doubleDark,
                    "&:hover": {
                      color: theme.palette.primary.dark,
                    },
                  }}
                >
                  log in
                </Button>
              </Link>
              <Link to="/register">
                <Button variant="contained" color="secondary">
                  sign up
                </Button>
              </Link>
            </Icons>
          )}
          {user?.id && (
            <Icons sx={{ display: { xs: "none", sm: "inherit" } }}>
              <Typography variant="span">{user.firstName}</Typography>
              <Link to="/user">
                <Tooltip title="Profile">
                  <Avatar
                    alt={user.firstName}
                    src={user.photoUrl}
                    sx={{
                      width: "30px",
                      height: "30px",
                      bgcolor: theme.palette.secondary.main,
                    }}
                  />
                </Tooltip>
              </Link>
              <Link to="/favorites">
                <Tooltip title="Favorites">
                  <FavoriteIcon
                    sx={{
                      color: theme.palette.primary.doubleDark,
                      "&:hover": {
                        color: theme.palette.primary.dark,
                      },
                    }}
                  />
                </Tooltip>
              </Link>
              <Link to="/login" onClick={removeUser}>
                <Tooltip title="Log Out">
                  <LogoutIcon
                    sx={{
                      color: theme.palette.primary.doubleDark,
                      "&:hover": {
                        color: theme.palette.primary.dark,
                      },
                    }}
                  />
                </Tooltip>
              </Link>
            </Icons>
          )}
        </StyledToolbar>
      </Container>
    </AppBar>
  );
}

export default Navbar;
