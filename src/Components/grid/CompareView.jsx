import React, {useState} from 'react'
import {Card, CardContent, CardMedia, Typography} from "@mui/material"
import CompareItem from "./CompareItem";
import { useSelector } from "react-redux";
import { VrScansAPI } from "../../api/vrScansAPI";
import {useQuery} from "react-query"

const CompareView = () => {
    const compareItems = useSelector((state) => state.vrScansReducer.compareItems);
    const vrScans = useSelector((state) => state.vrScansReducer.vrScans);
    const itemsToDisplay = vrScans.filter((scan) => compareItems.includes(scan.id.toString()))
    const [industries, setIndustries] = useState([]);

    useQuery(["industries"], () => VrScansAPI.getAllItems("/industries"), {
        enabled: industries.length == 0,
        onSuccess: (data) => {
            setIndustries(data);
        },
    });

    return(
        <div className="com-wrapper">
            <Typography variant="h4">Compare VR scans</Typography>
            <div className="items-card">
            <Card className="oneRow">
            <CardContent>
                <Typography variant="body2" fontWeight={500}>
                    Manifacturer: 
                </Typography>
                <Typography variant="body2" fontWeight={500}>
                    Type:
                </Typography>
                <Typography variant="body2" fontWeight={500}>
                    Industries:
                </Typography>
                <Typography variant="body2" fontWeight={500}>
                    Tags:
                </Typography>
                <Typography variant="body2" fontWeight={500}>
                    Colors:
                </Typography> 
            </CardContent>
            </Card>
            {itemsToDisplay.map((item) => (
             <CompareItem item={item} industries={industries} key={item.id} />
            ))}
            </div>
        </div>)
}

export default CompareView;