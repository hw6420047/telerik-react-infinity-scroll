import React, { lazy, useEffect, useState, useRef } from "react";
import { Box, Grid } from "@mui/material";
import { useSelector } from "react-redux";
import { Suspense } from "react";
import CompareBar from "./CompareBar"

const GridItem = lazy(() => import("./GridItem"));

function GridSection() {
    const manufacturers = useSelector((state) => state.vrScansReducer.manufacturers);
    const filteredVrScans = useSelector((state) => state.vrScansReducer.filteredVrScans);
    const compareItems = useSelector((state) => state.vrScansReducer.compareItems);

    const refIndex = useRef(0);
    const [currentElements, setCurrentElements] = useState([]);
    const chunkSize = 12;

    const handleScroll = (e) => {
        const scrollHeight = e.target.documentElement.scrollHeight;
        const currentHeight =
            e.target.documentElement.scrollTop + window.innerHeight;
        if (currentHeight + 1 >= scrollHeight) {
            const newIndex = refIndex.current + chunkSize;
            refIndex.current = newIndex;

            let newItems = [...filteredVrScans].slice(newIndex, newIndex + chunkSize);
            newItems = [...currentElements, ...newItems];

            setCurrentElements(newItems);
        }
    };

    useEffect(() => {
        window.addEventListener("scroll", handleScroll);
        return () => window.removeEventListener("scroll", handleScroll);
    }, [currentElements]);

    useEffect(() => {
        const newItems = [...filteredVrScans].slice(0, 0 + chunkSize);
        setCurrentElements(newItems);

        refIndex.current = chunkSize;
    
    }, [filteredVrScans]);


    return (
        <Box flex={3} sx={{ p: 1 }}>
         {compareItems.length > 1 ? <CompareBar/> : ''}
            <Suspense fallback={<div>isLoading...</div>}>
                <Grid container spacing={2}>
                    {currentElements.map((entity) => (
                        <Grid item xs={12} sm={6} md={4} lg={3} key={entity.id}>
                            <GridItem
                                key={entity.id}
                                entity={entity}
                                manufacturers={manufacturers}
                                isCompareOptionOn={true}
                            />
                        </Grid>
                    ))}
                </Grid>
            </Suspense>
        </Box>
    );
}

export default GridSection;
