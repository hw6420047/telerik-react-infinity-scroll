import React from 'react';
import { useSelector } from "react-redux";
import {  Avatar } from "@mui/material";
import { Link } from "react-router-dom";

const CompareBar = () => {
const compareItems = useSelector((state) => state.vrScansReducer.compareItems);
const vrScans = useSelector((state) => state.vrScansReducer.vrScans);
const itemsToDisplay = vrScans.filter((scan) => compareItems.includes(scan.id.toString()))

 return (
    <>
    <Link to="/compare" className='grey-link center-vertical' >Compare</Link>
    {itemsToDisplay.map((entity) => (
        <Avatar className='oneRow' alt={entity.name} key={"small-" + entity.id} src={entity.thumb} />
    ))}
    </>
 )
}

export default CompareBar;