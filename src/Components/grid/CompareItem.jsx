import React from 'react'
import {Card, CardContent, CardMedia, Typography} from "@mui/material"
import { useSelector } from "react-redux";

const CompareItem = (props) => {
    const colors = useSelector((state) => state.filtersReducer.colors);
    const types = useSelector((state) => state.filtersReducer.types);
    const tags = useSelector((state) => state.filtersReducer.tags);
    const manufacturers = useSelector((state) => state.vrScansReducer.manufacturers);

    const colorNames = colors.filter((c) => props.item.colors.includes(c.id));
    const typeName = types.filter((t) => props.item.materialTypeId === t.id);
    const manifacturer = manufacturers.filter((m) => props.item.manufacturerId === m.id);
    const tagsName = tags.filter((t) => props.item.tags.includes(t.id));
    const industriesName = props.industries.filter((i) => props.item.industries.includes(i.id));


    return (
        <Card className="oneRow">
                <CardMedia
                component="img"
                height="150px"
                image={props.item.thumb}
                alt={props.item.name}
                sx={{ maxWidth: "150px", margin: "0 auto" }}
            />
            <CardContent>
                <Typography variant="body2" fontWeight={500}>
                {manifacturer.map((m) => m.name ) ?? '-'}
                </Typography>
                <Typography variant="body2" fontWeight={500}>
                {typeName.map((t) => t.name ) ?? '-'}
                </Typography>
                <Typography variant="body2" fontWeight={500}>
                    {industriesName.map((i) => i.name).join(', ') ?? '-'}
                </Typography>
                <Typography variant="body2" fontWeight={500}>
                    {tagsName.map((t) => t.name).join(', ') ?? '-'}
                </Typography>
                <Typography variant="body2" fontWeight={500}>
                    {colorNames.map((c) => c.name).join(', ') ?? '-'}
                </Typography> 
            </CardContent>
            </Card>
    )
}

export default CompareItem;