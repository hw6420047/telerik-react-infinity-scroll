import {
    Card,
    CardContent,
    CardHeader,
    CardMedia,
    Checkbox,
    Typography,
} from "@mui/material";
import FavoriteBorderOutlinedIcon from "@mui/icons-material/FavoriteBorderOutlined";
import FavoriteOutlinedIcon from "@mui/icons-material/FavoriteOutlined";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { VrScansAPI } from "../../api/vrScansAPI";
import { userActions } from "../../store/users-slice";
import { vrScansActions } from "../../store/vrScans-slice";

function GridItem(props) {
    const dispatch = useDispatch();
    const favorites = useSelector((state) => state.userReducer.favorites);
    const compareItems = useSelector((state) => state.vrScansReducer.compareItems);
    const favoritesItemsId = favorites.map((f) => f.vrscanId);
    const user = useSelector((state) => state.userReducer.user);
    let isChecked = compareItems.includes(props.entity.id.toString());

    const manufacturer = props.manufacturers.find(
        (m) => m.id == props.entity.manufacturerId,
    );
    let manufacturerName = "";
    if (manufacturer) {
        manufacturerName = manufacturer.name;
    }

    const toggleFavorites = (е) => {
        const target = е.target;
        let value = target.checked;
        let id = target.id;

        let action = "POST"
        if(value) {
            if(id != null) {
                id = parseInt(id, 10)
            }
            
            VrScansAPI.updateItem("/favorites", {
                 "vrscanId": id,
                 "userId": user.id
             }, action).then(result => addFavoritEntry(result));
        } else {
            const token = (JSON.parse(localStorage.getItem('user')).accessToken);
            const favObjId = favorites.find((f) => f.vrscanId.toString() == id);
            if(favObjId) {
            VrScansAPI.deleteItem("/favorites/" + favObjId.id, token).then(() => removeFavouriteEntry(favObjId.id));
            }
        }
    }

    const addFavoritEntry = (data) => {
        const updatedList = [...favorites, data]
        dispatch(userActions.setFavorites(updatedList));
    }

    const removeFavouriteEntry = (id) => {
        const updatedList = [...favorites].filter((f) => f.id != id);
        dispatch(userActions.setFavorites(updatedList));
    }

    const updateCompareList = (event) => {
        const target = event.target;
        let value = target.checked;
        let id = target.id;
        let newItems;

        if(value) {
            newItems = [...compareItems, id];
        } else {
            newItems = [...compareItems].filter((c) => c != id);
        }

        dispatch(vrScansActions.setCompareItems(newItems));
    }

    return (
        <Card>
            {user != null && user.email != null ? <CardHeader
                 action={
                     <Checkbox
                        checked= {favoritesItemsId.includes(props.entity.id)}
                        onChange={toggleFavorites}
                        icon={<FavoriteBorderOutlinedIcon />}
                        id = {(props.entity.id).toString()}
                        checkedIcon={
                            <FavoriteOutlinedIcon sx={{ color: "red" }} />
                        }
                    /> 
                } 
                title={props.entity.name}
                titleTypographyProps={{ variant: "h6", fontWeight: "500" }}
            /> : <CardHeader title={props.entity.name} titleTypographyProps={{ variant: "h6", fontWeight: "500" }}/>}
            <CardMedia
                component="img"
                height="20%"
                image={props.entity.thumb}
                alt={props.entity.name}
                sx={{ maxWidth: "70%", margin: "0 auto" }}
            />
            <CardContent>
                <Typography variant="body2" fontWeight={500}>
                    {manufacturerName}
                </Typography>
                <Typography variant="body2">{props.entity.fileName}</Typography>
               {props.isCompareOptionOn ? <Typography variant="body2">Compare <input id={props.entity.id} checked={isChecked} onChange={(event) => updateCompareList(event)} type="checkbox"/>
                </Typography> : ''}
            </CardContent>
        </Card>
    );
}

export default GridItem;
