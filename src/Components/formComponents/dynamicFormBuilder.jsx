import React from "react";
import InputField from "./inputField";
import { useState } from "react";
import { useMutation } from "react-query";
import { VrScansAPI } from "../../api/vrScansAPI";
import {
    Avatar,
    Box,
    Button,
    Container,
    Grid,
    Link,
    Typography,
} from "@mui/material";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";

const DynamicFormBuilder = (props) => {
    const [inputs, setInputs] = useState({});
    const formJSON = props.formJSONsignup;
    const { mutate } = useMutation(() =>
        VrScansAPI.createUser("/users", inputs),
    );

    const renderElement = (formElement) =>
        InputField(formElement, handleInputChange);

    const handleInputChange = (event) => {
        const target = event.target;
        let value = target.value;
        const id = target.id;

        setInputs((values) => ({ ...values, [id]: value }));
    };

    const handleSubmit = (e) => {
        e.preventDefault();

        let result = JSON.stringify(
            Object.entries(inputs).map((s) => ({ [s[0]]: s[1] })),
        );

        inputs.photoUrl = "";

        // TODO reset fields and navigate

        mutate(inputs);
        console.log({ ...inputs, photoUrl: "" }, result);
    };
    //  sx={{ m: "0 auto" }}
    return (
        <Container component="main" maxWidth="xs">
            <Box
                sx={{
                    marginTop: 8,
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "center",
                }}
            >
                <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
                    <LockOutlinedIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                    Sign up
                </Typography>
                <Box
                    component="form"
                    noValidate
                    sx={{ mt: 3 }}
                    onSubmit={handleSubmit}
                >
                    <Grid container spacing={2}>
                        {formJSON.fields.map((e) => renderElement(e))}
                    </Grid>
                    <Button
                        type="submit"
                        fullWidth
                        color="secondary"
                        variant="contained"
                        sx={{ mt: 3, mb: 2 }}
                        href="/"
                    >
                        Sign Up
                    </Button>
                    <Grid container justifyContent="flex-end">
                        <Grid item>
                            <Link href="/login" variant="body2">
                                Already have an account? Log in
                            </Link>
                        </Grid>
                    </Grid>
                </Box>
            </Box>
        </Container>
    );
};

export default DynamicFormBuilder;
