import { Grid, TextField } from "@mui/material";
import React from "react";

const InputField = (inputElement, handleInputChange) => {
    const id = inputElement.id;

    return (        
            <Grid item className="form-group" key={id + "-wrapper"} xs={12}>
                <TextField
                    id={id}
                    label={inputElement.label}
                    type={inputElement.type}
                    key={id + "-element"}
                    onChange={handleInputChange}
                    // className="form-control"
                    autoComplete="given-name"
                    name="firstName"
                    size="small"
                    required
                    fullWidth
                />
            </Grid>
    );
};

export default InputField;
