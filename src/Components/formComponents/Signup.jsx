import React from "react";
import InputField from "./inputField";
import { useState } from "react";
import { useMutation } from "react-query";
import { VrScansAPI } from "../../api/vrScansAPI";
import {
  Alert,
  Avatar,
  Box,
  Button,
  Container,
  Grid,
  Typography,
} from "@mui/material";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import { theme } from "../../theme";
import { Link, useNavigate } from "react-router-dom";

const Signup = (props) => {
  const navigate = useNavigate();
  const [inputs, setInputs] = useState({});
  const [errorMsg, setErrorMsg] = useState("");
  const formJSON = props.formJSONsignup;
  const { mutate } = useMutation({
    mutationFn: (inputs) => {
      return VrScansAPI.createUser("/users", inputs);
    },
    onSuccess: (data) => {
      navigate("/login");
      return data;
    },
    onError: (data) => setErrorMsg(data.response.data),
  });

  const renderElement = (formElement) =>
    InputField(formElement, handleInputChange);

  const handleInputChange = (event) => {
    const target = event.target;
    let value = target.value;
    const id = target.id;

    setInputs((values) => ({ ...values, [id]: value }));
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    inputs.photoUrl = "";

    mutate(inputs);
  };
  
  return (
    <Container component="main" maxWidth="xs">
      <Box
        sx={{
          marginTop: 8,
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign up
        </Typography>
        <Box component="form" noValidate={false} sx={{ mt: 3 }} onSubmit={handleSubmit}>
          <Grid container spacing={2}>
            {formJSON.fields.map((e) => renderElement(e))}
          </Grid>
          <Button
            type="submit"
            fullWidth
            color="secondary"
            variant="contained"
            sx={{ mt: 3, mb: 2 }}
          >
            Sign Up
          </Button>
          <Grid container justifyContent="flex-end">
            <Grid item>
              <Link
                to="/login"
                style={{ color: theme.palette.primary.doubleDark }}
              >
                Already have an account? Log in
              </Link>
            </Grid>
          </Grid>
          {errorMsg && (
            <Alert variant="outlined" severity="error" sx={{ mt: 1 }}>
              {errorMsg}
            </Alert>
          )}
        </Box>
      </Box>
    </Container>
  );
};

export default Signup;
