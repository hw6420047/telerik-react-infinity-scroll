import React from "react";
import InputField from "./inputField";
import { useState } from "react";
import { useMutation } from "react-query";
import { VrScansAPI } from "../../api/vrScansAPI";
import { userActions } from "../../store/users-slice";
import {
  Alert,
  Avatar,
  Box,
  Button,
  Container,
  Grid,
  Typography,
} from "@mui/material";
import LoginIcon from "@mui/icons-material/Login";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router";
import { theme } from "../../theme";
import { Link } from "react-router-dom";

const Login = (props) => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [inputs, setInputs] = useState({});
  const [errorMsg, setErrorMsg] = useState("");
  const formJSON = props.formJSONlogin;
  const { mutate } = useMutation({
    mutationFn: (inputs) => {
      return VrScansAPI.loginUser("/logins", inputs);
    },
    onSuccess: (data) => {
      dispatch(userActions.setUser(data.user));
      navigate("/");
      return data;
    },
    onError: (data) => setErrorMsg(data.response.data),
  });

  const renderElement = (formElement) =>
    InputField(formElement, handleInputChange);

  const handleInputChange = (event) => {
    const target = event.target;
    let value = target.value;
    const id = target.id;

    setInputs((values) => ({ ...values, [id]: value }));
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    mutate(inputs);
    console.log(errorMsg);
    // console.log(msg);
  };

  return (
    <Container component="main" maxWidth="xs">
      <Box
        sx={{
          marginTop: 8,
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
          <LoginIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Log In
        </Typography>
        <Box component="form" noValidate sx={{ mt: 3 }} onSubmit={handleSubmit}>
          <Grid container spacing={2}>
            {formJSON.fields.map((e) => renderElement(e))}
          </Grid>
          <Button
            type="submit"
            fullWidth
            color="secondary"
            variant="contained"
            sx={{ mt: 3, mb: 2 }}
          >
            log in
          </Button>
          <Grid container justifyContent="flex-end">
            <Grid item>
              <Link
                to="/register"
                style={{ color: theme.palette.primary.doubleDark }}
              >
                Don&apos;t have an account? Sign Up
              </Link>
            </Grid>
          </Grid>
          {errorMsg && (
            <Alert variant="outlined" severity="error" sx={{ mt: 1 }}>
              {errorMsg}
            </Alert>
          )}
        </Box>
      </Box>
    </Container>
  );
};

export default Login;
