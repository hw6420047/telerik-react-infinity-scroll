import {
    Checkbox,
    FormControlLabel,
    FormGroup,
    styled,
    Typography,
} from "@mui/material";
import React from "react";

const CheckboxLabelStyled = styled(FormControlLabel)({
    "& .MuiSvgIcon-root": {
        fontSize: "0.9rem",
    },
    "& .MuiTypography-root": {
        fontSize: "0.9rem",
        lineHeight: 1,
    },
});

const CheckboxStyled = styled(Checkbox)({
    "& .MuiSvgIcon-root": {
        fontSize: "0.9rem",
    },
    "& .MuiTypography-root": {
        fontSize: "0.9rem",
    },
    "&.MuiButtonBase-root": {
        padding: "2px 8px",
    },
});

function BasicFilter(props) {
    return (
        <>
            <Typography variant="h6">{props.title}</Typography>
            <FormGroup>
                {props.entities.map((entity => 
                <CheckboxLabelStyled
                    key={entity.id}
                    control={<CheckboxStyled id={entity.id.toString()}/>}
                    label={entity.name}
                    onChange={props.onCheckboxClick}
                    checked={props.selected.includes(entity.id)}
                />
                ))}
            </FormGroup>
        </>
    );
}

export default BasicFilter;
