import { Checkbox, Stack, Tooltip } from "@mui/material";
import { Typography } from "@mui/material";
import React from "react";
import { useSelector } from "react-redux";

function ColorFilter(props) {
    const colors = useSelector((state) => state.filtersReducer.colors);
    const selectedColors = useSelector((state) => state.filtersReducer.selectedColors);

    const colorList = colors.map((c) => (
        <Tooltip key={c.key} title={c.name} placement="top-end">
            <Checkbox
                id={c.id.toString()}
                onChange={props.onCheckboxClick}
                key={c.id}
                checked={selectedColors.includes(c.id)}
                sx={{
                    padding: 0.5,
                    color: c.hex,
                    "&.Mui-checked": {
                        color: c.hex,
                    },
                    "& .MuiSvgIcon-root": {
                        borderRadius: 1
                    },
                }}
            />
        </Tooltip>
    ));

    return (
        <>
            <Typography variant="h6">Material Color</Typography>
            <Stack direction="row" flexWrap="wrap" sx={{ paddingRight: 4 }}>
                {colorList}
            </Stack>
        </>
    );
}

export default ColorFilter;