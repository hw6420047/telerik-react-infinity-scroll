import { Divider, Stack } from "@mui/material";
import React, { useEffect } from "react";
import ColorFilter from "./ColorFilter";
import SearchBar from "../shared/SearchBar";
import { VrScansAPI } from "../../api/vrScansAPI";
import { filtersActions } from "../../store/filters-slice";
import { vrScansActions } from "../../store/vrScans-slice"; 
import { useDispatch } from "react-redux";
import {useQuery} from "react-query";
import BasicFilter from "./BasicFilter";
import { useSelector } from "react-redux";

function FilterSection() {
    const dispatch = useDispatch();
    const colors = useSelector((state) => state.filtersReducer.colors);
    const types = useSelector((state) => state.filtersReducer.types);
    const tags = useSelector((state) => state.filtersReducer.tags);
    let selectedTypes = useSelector((state) => state.filtersReducer.selectedTypes);
    let selectedTags = useSelector((state) => state.filtersReducer.selectedTags);
    let selectedColors = useSelector((state) => state.filtersReducer.selectedColors);
    const inputValue = useSelector((state) => state.filtersReducer.inputSearchbox);
    const vrScans = useSelector((state) => state.vrScansReducer.vrScans);

        useQuery(["colors"], () => VrScansAPI.getAllItems('/colors'), {
            enabled: colors.length == 0,
            onSuccess: (data) => {
                dispatch(filtersActions.setColors(data));
            }
        });

        useQuery(["tags"], () => VrScansAPI.getAllItems('/tags'), {
            enabled: tags.length == 0,
            onSuccess: (data) => {
                dispatch(filtersActions.setTags(data));
            }
        });

        useQuery(["materials"], () => VrScansAPI.getAllItems('/materials'), {
            enabled: types.length == 0,
            onSuccess: (data) => {
                dispatch(filtersActions.setTypes(data));
            }
        });

        useEffect(() => {
            filterVrScans();
          }, [selectedColors, selectedTags, selectedTypes, inputValue])

    const filterVrScansByTag = (event) => {
        const target = event.target;
        const value = target.checked;
        const id = parseInt(target.id,10);

        if(value) {
            selectedTags = [...selectedTags, id];
        } else {
            selectedTags = selectedTags.filter((t) => t != id)
        }

        dispatch(filtersActions.setSelectedTags(selectedTags));
    }

    const filterVrScansByType = (event) => {
        const target = event.target;
        const value = target.checked;
        const id = parseInt(target.id,10);
        
        if(value) {
            selectedTypes = [...selectedTypes, id];
        } else {
            selectedTypes = selectedTypes.filter((t) => t != id)
        }

        dispatch(filtersActions.setSelectedTypes(selectedTypes));
    }

    const filterVrScansByColor = (event) => {
        const target = event.target;
        const value = target.checked;
        const id = parseInt(target.id,10);

        if(value) {
            selectedColors = [...selectedColors, id];
        } else {
            selectedColors = selectedColors.filter((c) => c != id)
        }

        dispatch(filtersActions.setSelectedColors(selectedColors));
    }

    const filterVrScansByName = (event) => {
        const target = event.target;
        const value = target.value;

        dispatch(filtersActions.setInputSearchbox(value));
    }

    const filterVrScans = () => {
        let vrScansFiltered = vrScans;

        if(inputValue.length > 0) {
            vrScansFiltered = vrScansFiltered.filter((scan) => scan.name.toLowerCase().includes(inputValue.toLowerCase()))
        }

        if(selectedTypes.length > 0) {
            vrScansFiltered = vrScansFiltered.filter((scan) => selectedTypes.includes(scan.materialTypeId));
        }

        if(selectedTags.length > 0) {
            vrScansFiltered = vrScansFiltered.filter((scan) => scan.tags.some((t) => selectedTags.includes(t)));
        } 

        if(selectedColors.length > 0) {
            vrScansFiltered = vrScansFiltered.filter((scan) => scan.colors.some((c) => selectedColors.includes(c)));
        } 

        dispatch(vrScansActions.setFilteredVrScans(vrScansFiltered));
    }

    return (
        <Stack
            direction="column"
            divider={<Divider flexItem />}
            spacing={1}
            justifyContent="flex-start"
            flex={1}
            p={1}
            sx={{
                display: { xs: "none", sm: "block" },
            }}
        >
            <SearchBar onChange={filterVrScansByName}/>
            <BasicFilter entities={types} title="Material Types" onCheckboxClick={filterVrScansByType} selected={selectedTypes}/>
            <ColorFilter onCheckboxClick={filterVrScansByColor}/>
            <BasicFilter entities={tags} title="Material Tags" onCheckboxClick={filterVrScansByTag} selected={selectedTags}/>
        </Stack>
    );
}

export default FilterSection;
