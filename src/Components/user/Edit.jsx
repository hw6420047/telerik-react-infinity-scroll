import React from "react";
import { useState } from "react";
import { useMutation } from "react-query";
import { VrScansAPI } from "../../api/vrScansAPI";
import {
  Alert,
  Avatar,
  Box,
  Button,
  Container,
  Grid,
  TextField,
  Typography,
} from "@mui/material";
import EditIcon from "@mui/icons-material/Edit";
import { useSelector } from "react-redux";

const Edit = () => {
  const asdasd = localStorage.getItem("user");
  const user = useSelector((state) => state.userReducer.user);
  const [inputs, setInputs] = useState({});
  const [errorMsg, setErrorMsg] = useState("");
  const { mutate } = useMutation({
    mutationFn: (inputs) => {
      return VrScansAPI.editUser(`/users/${user.id}`, inputs, user.accessToken);
    },
    onSuccess: (data) => {
      return data;
    },
    onError: (data) => {
      console.log(JSON.parse(asdasd.user));
      setErrorMsg(data.response.data);
    },
  });

  const handleInputChange = (event) => {
    const target = event.target;
    let value = target.value;
    const id = target.id;

    setInputs((values) => ({ ...values, [id]: value }));
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    mutate(inputs);
  };

  return (
    <Container component="main" maxWidth="xs">
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
          <EditIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Edit Profile
        </Typography>
        <Box component="form" noValidate sx={{ mt: 3 }} onSubmit={handleSubmit}>
          <Grid container spacing={2}>
            <Grid item className="form-group" xs={12}>
              <TextField
                id="firstName"
                label="First Name"
                type="text"
                onChange={handleInputChange}
                autoComplete="given-name"
                defaultValue={user.firstName}
                size="small"
                fullWidth
              />
            </Grid>
            <Grid item className="form-group" xs={12}>
              <TextField
                id="lastName"
                label="Last Name"
                type="text"
                onChange={handleInputChange}
                autoComplete="given-name"
                defaultValue={user.lastName}
                size="small"
                fullWidth
              />
            </Grid>
            <Grid item className="form-group" xs={12}>
              <TextField
                id="photoUrl"
                label="Photo URL"
                type="text"
                onChange={handleInputChange}
                autoComplete="given-name"
                defaultValue={user.photoUrl}
                size="small"
                fullWidth
              />
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            color="secondary"
            variant="contained"
            sx={{ mt: 3, mb: 2 }}
          >
            Save Changes
          </Button>
          {errorMsg && (
            <Alert variant="outlined" severity="error" sx={{ mt: 1 }}>
              {errorMsg}
            </Alert>
          )}
        </Box>
      </Box>
    </Container>
  );
};

export default Edit;
