import React from "react";
import { useSelector } from "react-redux";
import { Grid, Stack, Typography } from "@mui/material";
import GridItem from "../grid/GridItem";

const Favorites = () => {
  const favorites = useSelector((state) => state.userReducer.favorites);
  const vrScans = useSelector((state) => state.vrScansReducer.vrScans);
  const manufacturers = useSelector(
    (state) => state.vrScansReducer.manufacturers,
  );
  const favoritesItemsId = favorites.map((f) => f.vrscanId);
  const itemsGet = vrScans.filter((scan) => favoritesItemsId.includes(scan.id));

  return (
    <div className="fav">
      <Typography variant="h4" sx={{ fontWeight: [500], justifyContent: "center", display: "flex" }}>
        Favorite VR Scans
      </Typography>
      <Stack
        justifyContent="flex-start"
        flex={1}
        p={2}
      >
        <Grid container spacing={2}>
          {itemsGet.map((entity) => (
            <Grid item xs={12} sm={6} md={4} lg={3} key={entity.id}>
              <GridItem
                key={entity.id}
                entity={entity}
                manufacturers={manufacturers}
                isCompareOptionOn={false}
              />
            </Grid>
          ))}
        </Grid>
      </Stack>
    </div>
  );
};

export default Favorites;
