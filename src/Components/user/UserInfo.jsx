import React from "react";
import { useSelector } from "react-redux";
import {
  Card,
  CardContent,
  Avatar,
  CardHeader,
  Tooltip,
  IconButton,
  Typography,
} from "@mui/material";
import { theme } from "../../theme";
import EditIcon from "@mui/icons-material/Edit";
// import Edit from "./Edit";

const UserInfo = () => {
  const user = useSelector((state) => state.userReducer.user);
  //   const [edit, setEdit] = useState(false);

  //   const editRender = () => setEdit((prev) => !prev);

  return (
    <div className="centered">
      <Card>
        <CardHeader
          sx={{ height: 3 }}
          action={
            <Tooltip title="Edit Profile">
              <IconButton>
                <EditIcon
                  sx={{
                    color: theme.palette.primary.doubleDark,
                    "&:hover": {
                      color: theme.palette.primary.dark,
                    },
                  }}
                />
              </IconButton>
            </Tooltip>
          }
        />
        <Avatar
          alt={user != null ? user.firstName : "user photo"}
          src={user.photoUrl}
          sx={{
            margin: "0 auto",
            width: "300px",
            height: "300px",
            bgcolor: theme.palette.secondary.main,
          }}
        />
        <CardContent sx={{ mt: 1 }}>
          <Typography variant="body1">
            Name: {user != null ? user.firstName : ""}{" "}
            {user != null ? user.lastName : ""}
          </Typography>
          <Typography variant="body1">
            Email: {user != null ? user.email : ""}
          </Typography>
        </CardContent>
      </Card>
    </div>
  );
};

export default UserInfo;
