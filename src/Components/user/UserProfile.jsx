import React from "react";
import UserInfo from "./UserInfo";
import Favorites from "./Favorites";
import { Grid } from "@mui/material";

const UserProfile = () => {
  return (
    <div className="mainContainer">
      <Grid>
        <UserInfo />
        <hr />
        <Favorites />
      </Grid>
    </div>
  );
};

export default UserProfile;
