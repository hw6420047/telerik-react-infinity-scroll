import {createSlice} from '@reduxjs/toolkit'

const filtersSlice = createSlice({
    name: 'filters',
    initialState: {
        colors: [],
        types: [],
        tags:[], 
        selectedColors: [],
        selectedTypes: [],
        selectedTags:[],
        inputSearchbox: ''
    },
    reducers: {
        setColors(state, action) {
            state.colors = action.payload;
        },

        setTags(state, action) {
            state.tags = action.payload;
        },

        setTypes(state, action) {
            state.types = action.payload;
        },

        setSelectedColors(state, action) {
            state.selectedColors = action.payload;
        },

        setSelectedTypes(state, action) {
            state.selectedTypes = action.payload;
        },

        setSelectedTags(state, action) {
            state.selectedTags = action.payload;
        },

        setInputSearchbox(state, action) {
            state.inputSearchbox = action.payload;
        }
    }
});

export const filtersActions = filtersSlice.actions;
export default filtersSlice;