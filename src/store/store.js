import { configureStore } from '@reduxjs/toolkit'
import vrScansSlice from './vrScans-slice';
import filtersSlice from './filters-slice';
import userSlice from './users-slice';

export const store = configureStore({
  reducer: {
    vrScansReducer: vrScansSlice.reducer,
    filtersReducer: filtersSlice.reducer,
    userReducer: userSlice.reducer
  },
})

export default store;