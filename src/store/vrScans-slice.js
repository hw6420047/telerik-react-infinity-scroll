import {createSlice} from '@reduxjs/toolkit'

const vrScansSlice = createSlice({
    name: 'vrScans',
    initialState: {
        vrScans: [],
        manufacturers: [],
        filteredVrScans: [],
        compareItems: []
    },
    reducers: {
        setVRScans(state, action) {
            state.vrScans = action.payload;
        },

        setManufacturers(state, action) {
            state.manufacturers = action.payload;
        },

        setFilteredVrScans(state, action) {
            state.filteredVrScans = action.payload;
        },

        setCompareItems(state, action) {
            state.compareItems = action.payload;
        }
    }
});



export const vrScansActions = vrScansSlice.actions;
export default vrScansSlice;