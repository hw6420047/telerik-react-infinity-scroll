import { createSlice } from "@reduxjs/toolkit";

const userSlice = createSlice({
	name: 'users',
	initialState: {
		 users: [],
		 favorites: [],
		 user: {},
	},
	reducers: {
		 setUsers(state, action) {
			  state.users = action.payload;
		 },
		 setFavorites(state, action) {
			state.favorites = action.payload;
		 },
		 setUser(state, action) {
			state.user = action.payload;
		 },
	}
});

export const userActions = userSlice.actions;
export default userSlice;
