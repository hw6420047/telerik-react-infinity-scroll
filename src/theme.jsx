import { createTheme } from "@mui/material";
import { blueGrey, cyan } from "@mui/material/colors";

export const theme = createTheme({
  palette: {
    primary: {
      light: blueGrey[50],
      middle: blueGrey[100],
      main: blueGrey[300],
      dark: blueGrey[500],
      doubleDark: blueGrey[700],
    },
    secondary: {
      light: cyan[100],
      main: cyan[300],
      dark: cyan[500],
      doubleDark: cyan[700],
    },
    background: {
      default: blueGrey[50],
    },
    text: {
      primary: blueGrey[700],
    },
    overrides: {
      MuiButton: {
        containedPrimary: {
          color: "white",
        },
      },
    },
  },
  typography: {
    allVariants: {
      color: blueGrey[700],
    },
  },
});
