"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _react = _interopRequireWildcard(require("react"));
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != _typeof(e) && "function" != typeof e) return { "default": e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && Object.prototype.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n["default"] = e, t && t.set(e, n), n; }
function _typeof(o) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) { return typeof o; } : function (o) { return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o; }, _typeof(o); }
function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, _toPropertyKey(descriptor.key), descriptor); } }
function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); Object.defineProperty(subClass, "prototype", { writable: false }); if (superClass) _setPrototypeOf(subClass, superClass); }
function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf ? Object.setPrototypeOf.bind() : function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }
function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }
function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }
function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }
function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf.bind() : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }
function _defineProperty(obj, key, value) { key = _toPropertyKey(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return _typeof(key) === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (_typeof(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); } // The Dynamic Form Builder should have a propery onSubmit in order to process form data submission.
// Binding to event example:
// <DynamicFormBuilder formJSON={formJSON} onSubmit={(data) => { console.log(data) }} />
//console.log should output form data - the field id and element value - [{"firstname":"Ivan"},{"lastname":"Petrov"},{"country":"Bulgaria"},{"subscribe":true}]
var DynamicFormBuilder = /*#__PURE__*/function (_Component) {
  _inherits(DynamicFormBuilder, _Component);
  var _super = _createSuper(DynamicFormBuilder);
  function DynamicFormBuilder(props) {
    var _this;
    _classCallCheck(this, DynamicFormBuilder);
    _this = _super.call(this, props);
    _defineProperty(_assertThisInitialized(_this), "renderElement", function (formElement) {
      var type = formElement.type;
      switch (type) {
        case 'text':
          return _this.renderText(formElement);
        case 'select':
          return _this.renderSelect(formElement);
        case 'checkbox':
          return _this.renderCheckbox(formElement);
        default:
          console.log('Unknown type ' + type);
          break;
      }
    });
    _defineProperty(_assertThisInitialized(_this), "renderText", function (textElement) {
      var id = textElement.id;
      if (!_this.state[id]) {
        _this.state[id] = '';
      }
      return /*#__PURE__*/_react["default"].createElement("div", {
        className: "form-group",
        key: id + "-wrapper"
      }, /*#__PURE__*/_react["default"].createElement("label", {
        htmlFor: id,
        key: id + "-label"
      }, textElement.label), textElement.required ? '*' : '', /*#__PURE__*/_react["default"].createElement("input", {
        type: "text",
        id: id,
        key: id + "-element",
        className: "form-control",
        onChange: _this.handleInputChange,
        placeholder: textElement.placeholder,
        value: _this.state[id],
        required: textElement.required
      }));
    });
    _defineProperty(_assertThisInitialized(_this), "renderSelect", function (selectElement) {
      var id = selectElement.id;
      if (!_this.state[id]) {
        _this.state[id] = selectElement.options[0] || '';
      }
      return /*#__PURE__*/_react["default"].createElement("div", {
        className: "form-group",
        key: id + "-wrapper"
      }, /*#__PURE__*/_react["default"].createElement("label", {
        key: id + "-label",
        htmlFor: id
      }, selectElement.label), selectElement.required ? '*' : '', /*#__PURE__*/_react["default"].createElement("select", {
        key: id + "-select-wrapper",
        className: "form-control",
        id: id,
        value: _this.state[id],
        onChange: _this.handleInputChange,
        required: selectElement.required
      }, selectElement.options.map(function (o) {
        return _this.renderOption(o.label);
      })));
    });
    _defineProperty(_assertThisInitialized(_this), "renderOption", function (optionValue) {
      return /*#__PURE__*/_react["default"].createElement("option", {
        key: optionValue
      }, optionValue);
    });
    _defineProperty(_assertThisInitialized(_this), "renderCheckbox", function (checkboxElement) {
      var id = checkboxElement.id;
      if (!_this.state[id]) {
        _this.state[id] = false;
      }
      return /*#__PURE__*/_react["default"].createElement("div", {
        key: id + '-group-wrapper'
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "form-group",
        key: id + "-wrapper"
      }, /*#__PURE__*/_react["default"].createElement("input", {
        key: id + "-element",
        className: "form-check-input",
        type: "checkbox",
        value: _this.state[id],
        id: id,
        onChange: _this.handleInputChange,
        required: checkboxElement.required
      }), /*#__PURE__*/_react["default"].createElement("label", {
        key: id + "-label",
        className: "form-check-label",
        htmlFor: id
      }, checkboxElement.required ? '*' : '', checkboxElement.label)), _this.renderEmailInput(_this.state[id], id));
    });
    _defineProperty(_assertThisInitialized(_this), "renderEmailInput", function (isVisible, checkBoxId) {
      if (checkBoxId === 'subscribe') {
        var id = checkBoxId + 'Email';
        if (!_this.state[id] || !isVisible) {
          _this.state[id] = '';
        }
        return /*#__PURE__*/_react["default"].createElement("div", {
          className: "form-group",
          key: id + "-wrapper",
          hidden: !isVisible
        }, /*#__PURE__*/_react["default"].createElement("label", {
          key: id + "-label",
          htmlFor: id
        }, "Email*"), /*#__PURE__*/_react["default"].createElement("input", {
          key: id + "-element",
          className: "form-control",
          type: "email",
          value: _this.state[id],
          id: id,
          onChange: _this.handleInputChange,
          required: isVisible
        }));
      }
    });
    _defineProperty(_assertThisInitialized(_this), "handleInputChange", function (event) {
      var target = event.target;
      var value = target.value;
      var id = target.id;
      if (target.type === 'checkbox') {
        value = !(target.value == 'true');
      }
      _this.setState(_defineProperty({}, id, value));
    });
    _defineProperty(_assertThisInitialized(_this), "handleSubmit", function (e) {
      e.preventDefault();
      var result = JSON.stringify(Object.entries(_this.state).map(function (s) {
        return _defineProperty({}, s[0], s[1]);
      }));
      _this.props.onSubmit(result);
    });
    _defineProperty(_assertThisInitialized(_this), "render", function () {
      var formJSON = _this.props.formJSON;
      return /*#__PURE__*/_react["default"].createElement("form", {
        onSubmit: _this.handleSubmit,
        key: "form"
      }, " ", formJSON.fields.map(function (e) {
        return _this.renderElement(e);
      }), " ", /*#__PURE__*/_react["default"].createElement("button", {
        key: "form-button",
        type: "submit",
        className: "btn btn-primary mb-2"
      }, "Submit"));
    });
    _this.state = {};
    _this.handleInputChange = _this.handleInputChange.bind(_assertThisInitialized(_this));
    return _this;
  }
  return _createClass(DynamicFormBuilder);
}(_react.Component);
var _default = exports["default"] = DynamicFormBuilder;