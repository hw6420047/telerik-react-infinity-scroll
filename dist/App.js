"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _logo = _interopRequireDefault(require("./logo.svg"));
require("./App.css");
var _formData = _interopRequireDefault(require("./formData.json"));
var _dynamicFormBuilder = _interopRequireDefault(require("./Components/dynamicFormBuilder"));
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
function App() {
  return /*#__PURE__*/React.createElement("div", {
    className: "container"
  }, /*#__PURE__*/React.createElement(_dynamicFormBuilder["default"], {
    formJSON: _formData["default"],
    onSubmit: function onSubmit(data) {
      console.log(data);
    }
  }));
}
var _default = exports["default"] = App;