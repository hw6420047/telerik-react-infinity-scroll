# VR Scans Catalog

Telerik Upskill Course Project

The application is a VR Scans Catalog where users can browse, filter, search, like and compare different scans. 

## Getting started

This project was built from scratch. It can be started locally by running:

-   npm install
-   npm start

Client runs on port 3000.

## Public Part

Guest users have access to the Home Page. They can use the filter, search or compare options in the catalog.

Guest users can login or register using the respective pages.

## Private Part

Authenticated users can access the Home Page and its features, as well as like scans.